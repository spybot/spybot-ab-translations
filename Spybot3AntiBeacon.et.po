msgid ""
msgstr ""
"PO-Revision-Date: 2024-02-21 13:57+0000\n"
"Last-Translator: DeepL <noreply-mt-deepl@weblate.org>\n"
"Language-Team: Estonian <http://translations.spybot.de/projects/spybot-anti-beacon/spybot-anti-beacon/et/>\n"
"Language: et\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3"

#: antibeacon.branding.rsversiondescription
msgid "Stops telemetry of Windows, browsers, antivirus software and other apps."
msgstr "Peatab Windowsi, brauserite, viirusetõrjetarkvara ja muude rakenduste telemeetria."

#: antibeacon.controller.direct.rsantibeaconlicensebroken
msgid "Your license is broken, please re-install it!"
msgstr "Teie litsents on katki, palun installige see uuesti!"

#: antibeacon.controller.direct.rsantibeaconlicenseexpired
msgid "Your license has expired!"
msgstr "Teie litsents on aegunud!"

#: antibeacon.controller.direct.rsantibeaconlicensefree
msgid "Free version; buy Plus to get complete protection!"
msgstr "Tasuta versioon; täieliku kaitse saamiseks osta Plus!"

#: antibeacon.controller.direct.rsantibeaconlicenseinvalidproduct
#, object-pascal-format
msgid "Your license is meant for a different product (%0:s)!"
msgstr "Teie litsents on mõeldud teise toote jaoks (%0:s)!"

#: antibeacon.controller.direct.rsdisplaytextfirewalldefinition
msgid "Firewall: "
msgstr "Tulemüür: "

#: antibeacon.controller.direct.rsdisplaytextipdefinition
msgid "IPs: "
msgstr "IP-d: "

#: antibeacon.controller.direct.rsdisplaytextjsondefinition
msgid "JSON: "
msgstr "JSON: "

#: antibeacon.controller.direct.rsdisplaytextkeyvaluepair
msgid "KeyValuePair: "
msgstr "KeyValuePair: "

#: antibeacon.controller.direct.rsdisplaytextmozillaconfig
msgid "Mozilla Config: "
msgstr "Mozilla Config: "

#: antibeacon.controller.direct.rsdisplaytextregistrydefinition
msgid "Registry: "
msgstr "Register: "

#: antibeacon.controller.direct.rsdisplaytextservicedefinitionsimple
msgid "Service: "
msgstr "Teenindus: "

#: antibeacon.controller.direct.rsdisplaytextstaskdefinition
msgid "Task: "
msgstr "Ülesanne: "

#: antibeacon.controller.direct.rstelemetrymonitoractive
msgctxt "antibeacon.controller.direct.rstelemetrymonitoractive"
msgid "Live Monitor active"
msgstr "Live Monitor aktiivne"

#: antibeacon.controller.direct.rstelemetrymonitornotinstalled
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotinstalled"
msgid "Live Monitor not installed"
msgstr "Live Monitor ei ole paigaldatud"

#: antibeacon.controller.direct.rstelemetrymonitornotrunning
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotrunning"
msgid "Live Monitor not running"
msgstr "Live Monitor ei tööta"

#: antibeacon.controller.direct.rstelemetrymonitornotworking
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotworking"
msgid "Live Monitor failing"
msgstr "Live Monitor ebaõnnestub"

#: antibeacon.controller.direct.rstelemetrymonitorpending
msgctxt "antibeacon.controller.direct.rstelemetrymonitorpending"
msgid "Live Monitor change pending..."
msgstr "Live Monitor muutus ootab..."

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname
msgid "Refresh Anti-Beacon immunization"
msgstr "Värskenda Beacon-vastast immuniseerimist"

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname2
#, object-pascal-format
msgid "Refresh %s immunization"
msgstr "Värskenda %s immuniseerimine"

#: antibeacon.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will refresh your immunization against telemetry and tracking."
msgstr "See ülesanne värskendab teie immuniseerimist telemeetria ja jälgimise vastu."

#: antibeacon.ui.form.rslivemonitorblockedcount
#, object-pascal-format
msgid "%d of %d blocked."
msgstr "%d %d blokeeritud."

#: antibeacon.ui.form.rslivemonitornothingmonitoredyet
msgid "Nothing monitored yet."
msgstr "Midagi ei ole veel jälgitud."

#: antibeacon.ui.form.rsmenubarmonitor
msgctxt "antibeacon.ui.form.rsmenubarmonitor"
msgid "Live Monitor"
msgstr "Live Monitor"

#: antibeacon.ui.form.rsmenubarrecalldata
msgid "Windows Recall"
msgstr "Windowsi tagasikutsumine"

#: antibeacon.ui.form.rsmenubarsearchengines
msgid "Search Providers"
msgstr "Otsi teenusepakkujaid"

#: antibeacon.ui.form.rsspybot2licenseheader
msgid "Replace expired or broken license"
msgstr "Aegunud või katkise litsentsi asendamine"

#: antibeacon.ui.form.rsspybot2licenseprompt1
msgid "Your license is expired or broken. Do you want to replace it with this compatible one?"
msgstr "Teie litsents on aegunud või katki. Kas soovite selle asendada selle ühilduva litsentsiga?"

#: antibeacon.ui.form.rsspybot2licenseprompt2
#, object-pascal-format
msgid "%0:s license for %1:s, valid until %2:s?"
msgstr "%0:s litsents %1:s, kehtiv kuni %2:s?"

#: antibeacon.ui.form.rsspybot2licenseremember
msgid "Remember to ignore this license if I click \"No\"."
msgstr "Ärge unustage seda litsentsi, kui ma klõpsan \"Ei\"."

#: antibeacon.ui.form.rsstatusbuttonmonitor
msgid "Live Monitor initializing..."
msgstr "Live Monitor initsialiseerib..."

#: antibeacon.ui.frame.carbonfootprint.rsframecaption
msgid "About the Carbon Footprint of Telemetry"
msgstr "Telemetria süsiniku jalajälje kohta"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "luba"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon"
msgid "block"
msgstr "plokk"

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "Kirjeldusandmebaasis on selle kirje kohta vastuolulised andmed. Palun installige uuesti või kasutage ülaltoodud nuppu <i>Online</i>."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "Kirjeldusandmebaasis on selle kirje kohta vastuolulised andmed. Palun installige uuesti või kasutage ülaltoodud nuppu <i>Online</i>."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloaded
msgid "The descriptions database has not been loaded. Please re-install or use the <i>Online</i> button above."
msgstr "Kirjelduste andmebaasi ei ole laaditud. Palun installeerige uuesti või kasutage ülaltoodud nuppu <i>Online</i>."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloadednoonline
msgid "The descriptions database has not been loaded. Please re-install."
msgstr "Kirjelduste andmebaasi ei ole laaditud. Palun installige uuesti."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsimmunizernotfound
msgid "This immunizer is not yet described in the local description database. Please use the <i>Online</i> button above."
msgstr "Seda immuniseerija ei ole veel kohaliku kirjelduse andmebaasis kirjeldatud. Palun kasutage ülaltoodud nuppu <i>Online</i>."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline"
msgid "Online"
msgstr "Online"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsdescription
msgid "Description"
msgstr "Kirjeldus"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsentries
msgid "Entry Information (for the technically inclined user)"
msgstr "Sisestusinfo (tehniliselt haritud kasutajale)"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsframename
msgid "Immunizer Details"
msgstr "Immuniseerija andmed"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisfree
msgid "This protection is available in the free and paid version of Anti-Beacon."
msgstr "See kaitse on saadaval Anti-Beacon'i tasuta ja tasulises versioonis."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisplus
msgid "This protection is only available in the paid version Spybot Anti-Beacon Plus."
msgstr "See kaitse on saadaval ainult tasulises versioonis Spybot Anti-Beacon Plus."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevel
msgid "Recommendation Level"
msgstr "Soovituse tase"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways"
msgid "Always"
msgstr "Alati"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelmaximal
msgid "Maximum Protection"
msgstr "Maksimaalne kaitse"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelminimal
msgid "Minimal Protection"
msgstr "Minimaalne kaitse"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnever
msgid "Not Recommended"
msgstr "Ei soovitata"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone"
msgid "None"
msgstr "Puudub"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelrecommended
msgid "Recommended Protection"
msgstr "Soovitatav kaitse"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus"
msgid "Status"
msgstr "Staatus"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "See immuniseerija on saadaval ainult Spybot Anti-Beacon Plus'is."

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "luba"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon"
msgid "block"
msgstr "plokk"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroup3p
msgid "Third Party Analytics"
msgstr "Kolmanda osapoole analüütika"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupapps
msgid "Apps"
msgstr "Rakendused"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupav
msgid "Antivirus"
msgstr "Viirusetõrje"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupbrowser
msgid "Browser"
msgstr "Brauser"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupgames
msgid "Games"
msgstr "Mängud"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongrouphwdrives
msgid "Hardware Drivers"
msgstr "Riistvara draiverid"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoem
msgid "Preinstalled Manufacturer Software"
msgstr "Eelinstalleeritud tootja tarkvara"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoffice
msgid "Office Software"
msgstr "Kontoritarkvara"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupos
msgid "Operating System"
msgstr "Operatsioonisüsteem"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionapps
msgid "App Telemetry"
msgstr "Rakenduse telemeetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionappsinfree
msgid "App Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "App Telemetry - osta Spybot Anti-Beacon Plus, et neid saada!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsers
msgid "Browser Telemetry"
msgstr "Brauseri telemeetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree"
msgid "Browser Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Browser Telemetry - osta Spybot Anti-Beacon Plus, et neid saada!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptioncustominfree
#, object-pascal-format
msgid "%s - buy Spybot Anti-Beacon Plus to get these!"
msgstr "%s - osta Spybot Anti-Beacon Plus, et neid saada!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionmain
msgid "Microsoft Windows Telemetry"
msgstr "Microsoft Windows Telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplus
msgid "More Telemetry"
msgstr "Rohkem telemeetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplusinfree
msgid "More Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Rohkem telemetriat - ostke Spybot Anti-Beacon Plus, et neid saada!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalled
msgid "Pre-Installed Manufacturer Telemetry"
msgstr "Eelinstalleeritud tootja telemeetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalledinfree
msgid "Pre-Installed Manufacturer Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Eelinstalleeritud tootja telemeetria - ostke Spybot Anti-Beacon Plus, et neid saada!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply"
msgid "Apply"
msgstr "Rakenda"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactioncancel
msgid "Cancel"
msgstr "Tühista"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionreset
msgid "Reset"
msgstr "Reset"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersavailable
msgid "immunizers available"
msgstr "saadaval olevad immuniseerijad"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersframecaption
msgid "Telemetry options"
msgstr "Telemeetria võimalused"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerslevel
msgid "Protection Presets ↓"
msgstr "Kaitse eelseadistused ↓"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways"
msgid "Always"
msgstr "Alati"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectmaximal
msgid "Full"
msgstr "Täielik"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectminimal
msgid "Minimal"
msgstr "Minimaalne"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnever
msgid "Never"
msgstr "Mitte kunagi"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone"
msgid "None"
msgstr "Puudub"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectrecommended
msgid "Recommended"
msgstr "Soovitatav"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerunavailable
msgid "not found on your system"
msgstr "ei leitud teie süsteemis"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "See immuniseerija on saadaval ainult Spybot Anti-Beacon Plus'is."

#: antibeacon.ui.frame.livemonitor.rsablivemonitorcarboninfo
msgid "Carbon..."
msgstr "Süsinik..."

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespan
msgid "Display Range ↓"
msgstr "Näidikuala ↓"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespansession
msgid "Current Session (since system start)"
msgstr "Praegune seanss (alates süsteemi käivitamisest)"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespantotal
msgid "Total (since installation)"
msgstr "Kokku (alates paigaldamisest)"

#: antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption
msgctxt "antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption"
msgid "Live Monitor"
msgstr "Live Monitor"

#: antibeacon.ui.frame.livemonitor.rslivemonitorservicetableentry
msgid "Live Monitor,,"
msgstr "Live Monitor,,"

#: antibeacon.ui.frame.livemonitor.rslivemonitortableconfiguration
msgid "Configuration"
msgstr "Konfiguratsioon"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitoractiveprompt
msgid "Telemetry Monitor is working fine; do you want to stop it anyway?"
msgstr "Telemetriamonitor töötab suurepäraselt; kas soovite selle ikkagi peatada?"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotinstalledaction
msgid "click to install"
msgstr "paigaldamiseks klõpsa"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotrunningaction
msgid "click to start"
msgstr "klõpsa alustamiseks"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotworkingaction
msgid "click to restart"
msgstr "klõpsake taaskäivitamiseks"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitlesession
msgid "telemetry connection attempts seen since started"
msgstr "telemetriaühenduse katsed alates alustamisest"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitletotal
msgid "telemetry connection attempts seen ever"
msgstr "telemeetriaühenduse katsed, mida on kunagi nähtud"

#: antibeacon.ui.frame.main.rsantibeaconcount
msgid "Blocked telemetry options"
msgstr "Blokeeritud telemeetria võimalused"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4
#, object-pascal-format
msgid "%0:d out of %1:d protected"
msgstr "%0:d välja %1:d kaitstud"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4unlicensed
#, object-pascal-format
msgid "%0:d out of %1:d protected - %2:d require %3:s Plus"
msgstr "%0:d välja %1:d kaitstud - %2:d nõuavad %3:s Plus"

#: antibeacon.ui.frame.main.rsantibeaconcustomize
msgid "Customize"
msgstr "Kohandada"

#: antibeacon.ui.frame.main.rsantibeaconlastaction
msgid "Last action"
msgstr "Viimane tegevus"

#: antibeacon.ui.frame.main.rsantibeaconlicense
msgctxt "antibeacon.ui.frame.main.rsantibeaconlicense"
msgid "License"
msgstr "Litsents"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonbuy
msgid "click here to buy a license"
msgstr "Aegunud või katkise litsentsi asendamine"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonrenew
msgid "click to renew your License"
msgstr "klõpsake oma litsentsi uuendamiseks"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuy
msgid "License (click here to buy)"
msgstr "Litsents (ostmiseks klõpsa siia)"

#: antibeacon.ui.frame.main.rsantibeaconlicenserenew
msgid "License (click here to renew)"
msgstr "Litsents (uuendamiseks klõpsake siin)"

#: antibeacon.ui.frame.main.rsantibeaconlive
msgctxt "antibeacon.ui.frame.main.rsantibeaconlive"
msgid "Live Monitor"
msgstr "Live Monitor"

#: antibeacon.ui.frame.main.rsantibeaconmissinglibraries
#, object-pascal-format
msgid "%0:s missing"
msgstr "%0:s puuduv"

#: antibeacon.ui.frame.main.rsantibeaconprotecedmorecount
#, object-pascal-format, badformat
msgid "+ %0:d protected!"
msgstr "+ %0:d kaitstud"

#: antibeacon.ui.frame.main.rsantibeaconprotect
msgid "Protect"
msgstr "Kaitse"

#: antibeacon.ui.frame.main.rsantibeacontoasttitle
msgid "Protection Status"
msgstr "Kaitse staatus"

#: antibeacon.ui.frame.main.rsantibeacontranslationerror
msgid "There might be an issue with your translation. Please check for program updates."
msgstr "Teie tõlkega võib olla probleem. Palun kontrollige programmi uuendusi."

#: antibeacon.ui.frame.main.rsantibeaconunprotect
msgid "Unprotect"
msgstr "Kaitsmata"

#: antibeacon.ui.settings.rsantibeaconsettingsdownloadevaluationlicense
msgid "Check for available evaluation license on program start"
msgstr "Kontrollida, kas programmi käivitamisel on saadaval hindamislitsents"

#: antibeacon.ui.settings.rsantibeaconsettingsscheduledtaskcaption
msgid "Re-Immunize at each Logon (via Scheduled Task)"
msgstr "Taasimmuniseerimine iga sisselogimise ajal (planeeritud ülesande kaudu)"

#: antibeacon.ui.settings.rsantibeaconsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Kontrollige programmi käivitamisel uuendusi"

#: antirecall.ui.framedata.rsrecalldataactiondeletescreenshots
msgid "Delete screenshots"
msgstr "Ekraanipiltide kustutamine"

#: antirecall.ui.framedata.rsrecalldatacountscreenshot
msgid "Recorded screenshots found"
msgstr "Salvestatud ekraanipildid leitud"

#: antirecall.ui.framedata.rsrecalldatacounttextextracts
msgid "Extracted text snippets found"
msgstr "Väljavõetud tekstilõiked leitud"

#: antirecall.ui.framedata.rsrecalldatadbfound
msgid "Recall database(s) detected"
msgstr "Avastatud andmebaasi(de) tagasikutsumine"

#: antirecall.ui.framedata.rsrecalldatadetectedcount
msgid "data fragments within Recall detected"
msgstr "Recall'i raames tuvastatud andmefragmendid"

#: antirecall.ui.framedata.rsrecalldataframecaption
msgid "Microsoft Recall Data Overview"
msgstr "Microsofti tagasikutsumise andmete ülevaade"

#: antirecall.ui.framedata.rsrecalldatagrouppoliysystem
msgid "Windows AI Status (for complete system)"
msgstr "Windowsi AI staatus (kogu süsteemi puhul)"

#: antirecall.ui.framedata.rsrecalldatagrouppoliyuser
msgid "Windows AI Status (for current user)"
msgstr "Windows AI staatus (praeguse kasutaja jaoks)"

#: antirecall.ui.framedata.rsrecalldataloading
msgid "Analyzing Windows Recall data..."
msgstr "Windows Recall andmete analüüsimine..."

#: antirecall.ui.framedata.rsrecalldataloadingshort
msgctxt "antirecall.ui.framedata.rsrecalldataloadingshort"
msgid "Loading..."
msgstr "Laadimine..."

#: antirecall.ui.framedata.rsrecalldataprofilescount
msgid "Recall profiles detected"
msgstr "Avastatud tagasivõtuprofiilid"

#: antirecall.ui.framedata.rsrecalldatasettingdefault
msgid "Default"
msgstr "Vaikimisi"

#: antirecall.ui.framedata.rsrecalldatasettingdisabled
msgid "Disabled"
msgstr "Puudega inimesed"

#: antirecall.ui.framedata.rsrecalldatasettingenabled
msgid "Default (Enabled)"
msgstr "Vaikimisi (Lubatud)"

#: antirecall.ui.framedata.rsrecalldatasettingunknown
msgid "Unknown"
msgstr "Teadmata"

#: antirecall.ui.framedata.rsrecalldeletescreenshotsdialogmessage
#, object-pascal-format
msgid "Deleted %:0d of %:1d found Recall screenshots."
msgstr "Kustutatud %:0d %:1d-st leitud Recall ekraanipildid."

#: antirecall.ui.framedata.rsrecalltablecategoryprofiles
msgid "Profiles"
msgstr "Profiilid"

#: antirecall.ui.framedata.rsrecalltableentryprofiledetailsscreenshots
#, object-pascal-format
msgid "%d screenshots"
msgstr "%d ekraanipilte"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitor
msgid "Spybot Identity Monitor Preview"
msgstr "Spybot Identity Monitor eelvaade"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname"
msgid "Identity Monitor"
msgstr "Live Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorchecking
msgid "Testing for breaches..."
msgstr "Rikkumiste testimine..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorpending
msgid "..."
msgstr "..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitle
#, object-pascal-format
msgid "%0:d breaches found"
msgstr "%0:d leitud rikkumised"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitlenone
msgid "No breaches found"
msgstr "Rikkumisi ei leitud"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableclean
msgid "No breaches found. Use Identity Monitor to regularly check this and more accounts."
msgstr "Rikkumisi ei leitud. Kasutage Identity Monitor'i, et regulaarselt kontrollida seda ja teisi kontosid."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortablefound
msgid "Breaches found! Use Identity Monitor for details."
msgstr "Rikkumised leitud! Kasutage Identity Monitor'i üksikasjade jaoks."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableusercaption
msgid "Monitored user (in Identity Monitor, you can set up more)"
msgstr "Jälgitav kasutaja (Identity Monitoris saate määrata rohkem)"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelclean
msgid "Logon clean"
msgstr "Logon puhas"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfail
msgid "Failed"
msgstr "Ebaõnnestunud"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfound
msgid "Breaches found!"
msgstr "Rikkumised leitud!"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname"
msgid "Identity Monitor"
msgstr "Live Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheeltesting
msgid "Testing"
msgstr "Testimine"

#: spybot3.settings.ui.rsdarkmodesettingalwaysoff
msgid "Always off"
msgstr "Alati välja lülitatud"

#: spybot3.settings.ui.rsdarkmodesettingalwayson
msgid "Always on"
msgstr "Alati sisse lülitatud"

#: spybot3.settings.ui.rsdarkmodesettingnighttime
msgid "On during night time"
msgstr "Öösel sisse lülitatud"

#: spybot3.settings.ui.rsdarkmodesettingsystemsetting
msgid "Based on global system setting"
msgstr "Põhineb globaalsel süsteemi seadistusel"

#: spybot3.settings.ui.rsdarkmodesettingthreatlevel
msgid "Based on threat level"
msgstr "Ohutaseme alusel"

#: spybot3.settings.ui.rsproxymodecustomproxy
msgid "Use custom proxy"
msgstr "Kasutage kohandatud proxy't"

#: spybot3.settings.ui.rsproxymodenoproxy
msgid "Do not use a proxy"
msgstr "Ärge kasutage proxy't."

#: spybot3.settings.ui.rsproxymodesystemproxy
msgid "Use system proxy"
msgstr "Kasutage süsteemi proxy"

#: spybot3.settings.ui.rssettingsalwaysshowconnections
msgctxt "spybot3.settings.ui.rssettingsalwaysshowconnections"
msgid "Always show connections"
msgstr "Näita alati ühendusi"

#: spybot3.settings.ui.rssettingsdarkmode
msgid "Dark Mode"
msgstr "Tume režiim"

#: spybot3.settings.ui.rssettingsdebugloglog
msgid "Show debug information"
msgstr "Näita silumisandmeid"

#: spybot3.settings.ui.rssettingslanguagesystemdefault
msgid "System default language"
msgstr "Süsteemi vaikimisi keel"

#: spybot3.settings.ui.rssettingsproxymode
msgid "Proxy"
msgstr "Proxy"

#: spybot3.settings.ui.rssettingsshowmainmenu
msgid "Show assistive menu on Ctrl+M"
msgstr "Näita abistavat menüüd Ctrl+M"

#: spybot3.settings.ui.rssettingsshownotifications
msgid "Show notifications"
msgstr "Näita teateid"

#: spybot3.settings.ui.rssettingsusedyslexicfont
msgid "Improve text for dyslexic users"
msgstr "Parandada teksti düsleksikahäiretega kasutajate jaoks"

#: spybot3.settings.ui.rssettingsuserinterfacelanguage
msgid "User interface language"
msgstr "Kasutajaliidese keel"

#: spybot3.settings.ui.rssettingsusespeechapi
msgid "Speak user interface elements"
msgstr "Rääkige kasutajaliidese elementidest"

#: spybot3searchengines.database.rssearchengineenergystandard
msgid "bod standard"
msgstr "kehastandard"

#: spybot3searchengines.database.rssearchengineenergysustainable
msgid "sustainable"
msgstr "jätkusuutlik"

#: spybot3searchengines.database.rssearchengineprivacyfocus
msgid "privacy aware"
msgstr "eraelu puutumatuse kaitse"

#: spybot3searchengines.database.rssearchengineprivacystandard
msgid "standard"
msgstr "standard"

#: spybot3searchengines.database.rssearchenginetypeinstalled
msgid "installed"
msgstr "paigaldatud"

#: spybot3searchengines.database.rssearchenginetyperecommended
msgid "recommended"
msgstr "Soovitatav"

#: tformspybot3antibeacon.caption
msgid "Spybot AntiBeacon"
msgstr "Spybot AntiBeacon"

