msgid ""
msgstr ""
"Project-Id-Version: Portuguese (Portugal)\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-01-12 19:59+0000\n"
"Last-Translator: Apertium APy <noreply-mt-apertium-apy@weblate.org>\n"
"Language-Team: Portuguese (Portugal) <http://translations.spybot.de/projects/spybot-anti-beacon/spybot-anti-beacon/pt_PT/>\n"
"Language: pt_pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 5.3"

#: antibeacon.branding.rsversiondescription
msgid "Stops telemetry of Windows, browsers, antivirus software and other apps."
msgstr "Interrompe a telemetria do Windows, navegadores, software antivírus e outros aplicativos."

#: antibeacon.controller.direct.rsantibeaconlicensebroken
msgid "Your license is broken, please re-install it!"
msgstr "A sua licença está com problemas, por favor, reinstale-a!"

#: antibeacon.controller.direct.rsantibeaconlicenseexpired
msgid "Your license has expired!"
msgstr "A sua licença expirou!"

#: antibeacon.controller.direct.rsantibeaconlicensefree
msgid "Free version; buy Plus to get complete protection!"
msgstr "Versão gratuita; compre Plus para obter a proteção completa!"

#: antibeacon.controller.direct.rsantibeaconlicenseinvalidproduct
#, object-pascal-format
msgid "Your license is meant for a different product (%0:s)!"
msgstr "A sua licença é para um produto diferente (%0:s)!"

#: antibeacon.controller.direct.rsdisplaytextfirewalldefinition
msgid "Firewall: "
msgstr "Firewall: "

#: antibeacon.controller.direct.rsdisplaytextipdefinition
msgid "IPs: "
msgstr "IPs: "

#: antibeacon.controller.direct.rsdisplaytextjsondefinition
msgid "JSON: "
msgstr "JSON: "

#: antibeacon.controller.direct.rsdisplaytextkeyvaluepair
msgid "KeyValuePair: "
msgstr "KeyValuePair: "

#: antibeacon.controller.direct.rsdisplaytextmozillaconfig
msgid "Mozilla Config: "
msgstr "Mozilla Config: "

#: antibeacon.controller.direct.rsdisplaytextregistrydefinition
msgid "Registry: "
msgstr "Registo: "

#: antibeacon.controller.direct.rsdisplaytextservicedefinitionsimple
msgid "Service: "
msgstr "Serviço: "

#: antibeacon.controller.direct.rsdisplaytextstaskdefinition
msgid "Task: "
msgstr "Tarefa: "

#: antibeacon.controller.direct.rstelemetrymonitoractive
msgctxt "antibeacon.controller.direct.rstelemetrymonitoractive"
msgid "Live Monitor active"
msgstr "Live Monitor ativo"

#: antibeacon.controller.direct.rstelemetrymonitornotinstalled
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotinstalled"
msgid "Live Monitor not installed"
msgstr "Live Monitor não instalado"

#: antibeacon.controller.direct.rstelemetrymonitornotrunning
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotrunning"
msgid "Live Monitor not running"
msgstr "O Live Monitor não está a funcionar"

#: antibeacon.controller.direct.rstelemetrymonitornotworking
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotworking"
msgid "Live Monitor failing"
msgstr "Falha do monitor em direto"

#: antibeacon.controller.direct.rstelemetrymonitorpending
msgctxt "antibeacon.controller.direct.rstelemetrymonitorpending"
msgid "Live Monitor change pending..."
msgstr "Monitorização em direto pendente de alteração..."

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname
msgid "Refresh Anti-Beacon immunization"
msgstr "Recarregar imunização Anti-Beacon"

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname2
#, object-pascal-format
msgid "Refresh %s immunization"
msgstr "Atualizar a imunização %s"

#: antibeacon.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will refresh your immunization against telemetry and tracking."
msgstr "Esta tarefa irá recarregar a sua imunização contra telemetria e monitorização."

#: antibeacon.ui.form.rslivemonitorblockedcount
#, object-pascal-format
msgid "%d of %d blocked."
msgstr "%d de %d bloqueado."

#: antibeacon.ui.form.rslivemonitornothingmonitoredyet
msgid "Nothing monitored yet."
msgstr "Ainda não há nada monitorizado."

#: antibeacon.ui.form.rsmenubarmonitor
msgctxt "antibeacon.ui.form.rsmenubarmonitor"
msgid "Live Monitor"
msgstr "Monitor em direto"

#: antibeacon.ui.form.rsmenubarrecalldata
msgid "Windows Recall"
msgstr "Recuperação do Windows"

#: antibeacon.ui.form.rsmenubarsearchengines
msgid "Search Providers"
msgstr "Provedores de pesquisa"

#: antibeacon.ui.form.rsspybot2licenseheader
msgid "Replace expired or broken license"
msgstr "Substituir a licença expirada ou com problemas"

#: antibeacon.ui.form.rsspybot2licenseprompt1
msgid "Your license is expired or broken. Do you want to replace it with this compatible one?"
msgstr "A sua licença expirou ou está com problemas. Pretende substituí-la por esta compatível?"

#: antibeacon.ui.form.rsspybot2licenseprompt2
#, object-pascal-format
msgid "%0:s license for %1:s, valid until %2:s?"
msgstr "%0:s licença para %1:s, válida até %2:s?"

#: antibeacon.ui.form.rsspybot2licenseremember
msgid "Remember to ignore this license if I click \"No\"."
msgstr "Lembrar para ignorar esta licença se eu clicar em 'Não'."

#: antibeacon.ui.form.rsstatusbuttonmonitor
msgid "Live Monitor initializing..."
msgstr "Inicialização do Live Monitor..."

#: antibeacon.ui.frame.carbonfootprint.rsframecaption
msgid "About the Carbon Footprint of Telemetry"
msgstr "Sobre a Pegada de Carbono da Telemetria"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "permitir"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon"
msgid "block"
msgstr "bloquear"

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "A base de dados das descrições tem dados inconsistentes nesta entrada. Por favor, reinstale ou utilize o botão <i>On-line</i> em cima."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "A base de dados das descrições tem dados inconsistentes nesta entrada. Por favor, reinstale ou utilize o botão <i>On-line</i> em cima."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloaded
msgid "The descriptions database has not been loaded. Please re-install or use the <i>Online</i> button above."
msgstr "A base de dados das descrições não foi carregada. Por favor, reinstale ou utilize o botão <i>On-line</i> em cima."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloadednoonline
msgid "The descriptions database has not been loaded. Please re-install."
msgstr "A base de dados das descrições não foi carregada. Por favor, reinstale."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsimmunizernotfound
msgid "This immunizer is not yet described in the local description database. Please use the <i>Online</i> button above."
msgstr "Este imunizador ainda não está descrito na base de dados das descrições local. Por favor, utilize o botão <i>On-line</i> em cima."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline"
msgid "Online"
msgstr "On-line"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsdescription
msgid "Description"
msgstr "Descrição"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsentries
msgid "Entry Information (for the technically inclined user)"
msgstr "Informação da Entrada (para o utilizador tecnicamente inclinado)"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsframename
msgid "Immunizer Details"
msgstr "Detalhes do Imunizador"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisfree
msgid "This protection is available in the free and paid version of Anti-Beacon."
msgstr "Esta proteção está disponível na versão gratuita e paga do Anti-Beacon."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisplus
msgid "This protection is only available in the paid version Spybot Anti-Beacon Plus."
msgstr "Esta proteção só está disponível na versão paga do Spybot Anti-Beacon Plus."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevel
msgid "Recommendation Level"
msgstr "Nível de Recomendação"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways"
msgid "Always"
msgstr "Sempre"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelmaximal
msgid "Maximum Protection"
msgstr "Proteção Máxima"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelminimal
msgid "Minimal Protection"
msgstr "Proteção Mínima"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnever
msgid "Not Recommended"
msgstr "Não Recomendado"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone"
msgid "None"
msgstr "Nenhum"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelrecommended
msgid "Recommended Protection"
msgstr "Proteção Recomendada"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus"
msgid "Status"
msgstr "Estado"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Este imunizador está disponível apenas no Spybot Anti-Beacon Plus."

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "permitir"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon"
msgid "block"
msgstr "bloquear"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroup3p
msgid "Third Party Analytics"
msgstr "Analítica de Terceiros"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupapps
msgid "Apps"
msgstr "Aplicações"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupav
msgid "Antivirus"
msgstr "Antivírus"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupbrowser
msgid "Browser"
msgstr "Navegador"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupgames
msgid "Games"
msgstr "Jogos"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongrouphwdrives
msgid "Hardware Drivers"
msgstr "Controladores de Hardware"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoem
msgid "Preinstalled Manufacturer Software"
msgstr "Programa do Fabricante Pré-instalado"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoffice
msgid "Office Software"
msgstr "Programas de Escritório"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupos
msgid "Operating System"
msgstr "Sistema Operativo"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionapps
msgid "App Telemetry"
msgstr "App Telemetry"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionappsinfree
msgid "App Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "App Telemetry - compre Spybot Anti-Beacon Plus para obter estes!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsers
msgid "Browser Telemetry"
msgstr "Browser Telemetry"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree"
msgid "Browser Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Browser Telemetry - compre Spybot Anti-Beacon Plus para obter estes!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptioncustominfree
#, object-pascal-format
msgid "%s - buy Spybot Anti-Beacon Plus to get these!"
msgstr "%s - compre Spybot Anti-Beacon Plus para obter estes!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionmain
msgid "Microsoft Windows Telemetry"
msgstr "Telemetria do Microsoft Windows"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplus
msgid "More Telemetry"
msgstr "Mais Telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplusinfree
msgid "More Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Mais Telemetria - compre Spybot Anti-Beacon Plus para obter estes!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalled
msgid "Pre-Installed Manufacturer Telemetry"
msgstr "Telemetria Pré-Instalada do Fabricante"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalledinfree
msgid "Pre-Installed Manufacturer Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Telemetria Pré-Instalada Fabricante - compre Spybot Anti-Beacon Plus para obter estes!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply"
msgid "Apply"
msgstr "Aplicar"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactioncancel
msgid "Cancel"
msgstr "Cancelar"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionreset
msgid "Reset"
msgstr "Reiniciar"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersavailable
msgid "immunizers available"
msgstr "imunizadores disponíveis"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersframecaption
msgid "Telemetry options"
msgstr "Opções de telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerslevel
msgid "Protection Presets ↓"
msgstr "Pré-ajustes da Proteção ↓"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways"
msgid "Always"
msgstr "Sempre"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectmaximal
msgid "Full"
msgstr "Completa"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectminimal
msgid "Minimal"
msgstr "Mínima"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnever
msgid "Never"
msgstr "Nunca"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone"
msgid "None"
msgstr "Nenhuma"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectrecommended
msgid "Recommended"
msgstr "Recomendada"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerunavailable
msgid "not found on your system"
msgstr "não encontrado no seu sistema"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Este imunizador só está disponível no Spybot Anti-Beacon Plus."

#: antibeacon.ui.frame.livemonitor.rsablivemonitorcarboninfo
msgid "Carbon..."
msgstr "Carbono..."

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespan
msgid "Display Range ↓"
msgstr "Intervalo de visualização ↓"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespansession
msgid "Current Session (since system start)"
msgstr "Sessão atual (desde o início do sistema)"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespantotal
msgid "Total (since installation)"
msgstr "Total (desde a instalação)"

#: antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption
msgctxt "antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption"
msgid "Live Monitor"
msgstr "Monitor em direto"

#: antibeacon.ui.frame.livemonitor.rslivemonitorservicetableentry
msgid "Live Monitor,,"
msgstr "#/Orar @Monitor@,@,@"

#: antibeacon.ui.frame.livemonitor.rslivemonitortableconfiguration
msgid "Configuration"
msgstr "@Configuration@"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitoractiveprompt
msgid "Telemetry Monitor is working fine; do you want to stop it anyway?"
msgstr "O Monitor de telemetria está a funcionar bem; pretende pará-lo de qualquer forma?"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotinstalledaction
msgid "click to install"
msgstr "clique para instalar"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotrunningaction
msgid "click to start"
msgstr "clique para iniciar"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotworkingaction
msgid "click to restart"
msgstr "clique para reiniciar"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitlesession
msgid "telemetry connection attempts seen since started"
msgstr "tentativas de ligação por telemetria registadas desde o início"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitletotal
msgid "telemetry connection attempts seen ever"
msgstr "tentativas de ligação por telemetria jamais vistas"

#: antibeacon.ui.frame.main.rsantibeaconcount
msgid "Blocked telemetry options"
msgstr "Opções de telemetria bloqueadas"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4
#, object-pascal-format
msgid "%0:d out of %1:d protected"
msgstr "%0:d de %1:d protegida(s)"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4unlicensed
#, object-pascal-format
msgid "%0:d out of %1:d protected - %2:d require %3:s Plus"
msgstr "%0:d de %1:d protegida(s) - %2:d requer %3:s Plus"

#: antibeacon.ui.frame.main.rsantibeaconcustomize
msgid "Customize"
msgstr "Personal"

#: antibeacon.ui.frame.main.rsantibeaconlastaction
msgid "Last action"
msgstr "Última ação"

#: antibeacon.ui.frame.main.rsantibeaconlicense
msgctxt "antibeacon.ui.frame.main.rsantibeaconlicense"
msgid "License"
msgstr "Licença"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonbuy
msgid "click here to buy a license"
msgstr "Substituir a licença expirada ou com problemas"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonrenew
msgid "click to renew your License"
msgstr "clique para renovar sua licença"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuy
msgid "License (click here to buy)"
msgstr "Licença (clique aqui para comprar)"

#: antibeacon.ui.frame.main.rsantibeaconlicenserenew
msgid "License (click here to renew)"
msgstr "Licença (clique aqui para renovar)"

#: antibeacon.ui.frame.main.rsantibeaconlive
msgctxt "antibeacon.ui.frame.main.rsantibeaconlive"
msgid "Live Monitor"
msgstr "Monitor em direto"

#: antibeacon.ui.frame.main.rsantibeaconmissinglibraries
#, object-pascal-format
msgid "%0:s missing"
msgstr "%0:s em falta"

#: antibeacon.ui.frame.main.rsantibeaconprotecedmorecount
#, object-pascal-format, badformat
msgid "+ %0:d protected!"
msgstr "+ %0:d protegida(s)"

#: antibeacon.ui.frame.main.rsantibeaconprotect
msgid "Protect"
msgstr "Proteger"

#: antibeacon.ui.frame.main.rsantibeacontoasttitle
msgid "Protection Status"
msgstr "Estado da Proteção"

#: antibeacon.ui.frame.main.rsantibeacontranslationerror
msgid "There might be an issue with your translation. Please check for program updates."
msgstr "Pode haver um problema com a sua tradução. Por favor, procure por atualizações do programa."

#: antibeacon.ui.frame.main.rsantibeaconunprotect
msgid "Unprotect"
msgstr "Desproteger"

#: antibeacon.ui.settings.rsantibeaconsettingsdownloadevaluationlicense
msgid "Check for available evaluation license on program start"
msgstr "Verificar se há licença de avaliação disponível no início do programa"

#: antibeacon.ui.settings.rsantibeaconsettingsscheduledtaskcaption
msgid "Re-Immunize at each Logon (via Scheduled Task)"
msgstr "Voltar a imunizar em cada 'Sessão' (via «Tarefa Programada»)"

#: antibeacon.ui.settings.rsantibeaconsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Procurar por atualizações no arranque do programa"

#: antirecall.ui.framedata.rsrecalldataactiondeletescreenshots
msgid "Delete screenshots"
msgstr "Excluir capturas de tela"

#: antirecall.ui.framedata.rsrecalldatacountscreenshot
msgid "Recorded screenshots found"
msgstr "Capturas de tela gravadas encontradas"

#: antirecall.ui.framedata.rsrecalldatacounttextextracts
msgid "Extracted text snippets found"
msgstr "Trechos de texto extraídos encontrados"

#: antirecall.ui.framedata.rsrecalldatadbfound
msgid "Recall database(s) detected"
msgstr "Banco(s) de dados de recall detectado(s)"

#: antirecall.ui.framedata.rsrecalldatadetectedcount
msgid "data fragments within Recall detected"
msgstr "fragmentos de dados no Recall detectados"

#: antirecall.ui.framedata.rsrecalldataframecaption
msgid "Microsoft Recall Data Overview"
msgstr "Visão geral dos dados de recall da Microsoft"

#: antirecall.ui.framedata.rsrecalldatagrouppoliysystem
msgid "Windows AI Status (for complete system)"
msgstr "Status do Windows AI (para o sistema completo)"

#: antirecall.ui.framedata.rsrecalldatagrouppoliyuser
msgid "Windows AI Status (for current user)"
msgstr "Status do Windows AI (para o usuário atual)"

#: antirecall.ui.framedata.rsrecalldataloading
msgid "Analyzing Windows Recall data..."
msgstr "Analisando os dados do Windows Recall..."

#: antirecall.ui.framedata.rsrecalldataloadingshort
msgctxt "antirecall.ui.framedata.rsrecalldataloadingshort"
msgid "Loading..."
msgstr "Carregando..."

#: antirecall.ui.framedata.rsrecalldataprofilescount
msgid "Recall profiles detected"
msgstr "Perfis de recall detectados"

#: antirecall.ui.framedata.rsrecalldatasettingdefault
msgid "Default"
msgstr "Padrão"

#: antirecall.ui.framedata.rsrecalldatasettingdisabled
msgid "Disabled"
msgstr "Desativado"

#: antirecall.ui.framedata.rsrecalldatasettingenabled
msgid "Default (Enabled)"
msgstr "Padrão (Ativado)"

#: antirecall.ui.framedata.rsrecalldatasettingunknown
msgid "Unknown"
msgstr "Desconhecido"

#: antirecall.ui.framedata.rsrecalldeletescreenshotsdialogmessage
#, object-pascal-format
msgid "Deleted %:0d of %:1d found Recall screenshots."
msgstr "Eliminado %:0d de %:1d encontrado Recuperar capturas de tela."

#: antirecall.ui.framedata.rsrecalltablecategoryprofiles
msgid "Profiles"
msgstr "Perfis"

#: antirecall.ui.framedata.rsrecalltableentryprofiledetailsscreenshots
#, object-pascal-format
msgid "%d screenshots"
msgstr "%d capturas de tela"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitor
msgid "Spybot Identity Monitor Preview"
msgstr "Visualização do Spybot Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorchecking
msgid "Testing for breaches..."
msgstr "Testes para detectar violações..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorpending
msgid "..."
msgstr "..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitle
#, object-pascal-format
msgid "%0:d breaches found"
msgstr "%0:d violações encontradas"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitlenone
msgid "No breaches found"
msgstr "Não foram encontradas violações"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableclean
msgid "No breaches found. Use Identity Monitor to regularly check this and more accounts."
msgstr "Não foram encontradas violações. Use o Identity Monitor para verificar regularmente essa e outras contas."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortablefound
msgid "Breaches found! Use Identity Monitor for details."
msgstr "Violações encontradas! Use o Identity Monitor para obter detalhes."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableusercaption
msgid "Monitored user (in Identity Monitor, you can set up more)"
msgstr "Usuário monitorado (no Identity Monitor, você pode configurar mais)"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelclean
msgid "Logon clean"
msgstr "Logon limpo"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfail
msgid "Failed"
msgstr "Falha"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfound
msgid "Breaches found!"
msgstr "Violações encontradas!"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheeltesting
msgid "Testing"
msgstr "Testes"

#: spybot3.settings.ui.rsdarkmodesettingalwaysoff
msgid "Always off"
msgstr "Sempre desligado"

#: spybot3.settings.ui.rsdarkmodesettingalwayson
msgid "Always on"
msgstr "Sempre ligado"

#: spybot3.settings.ui.rsdarkmodesettingnighttime
msgid "On during night time"
msgstr "Ligado durante a noite"

#: spybot3.settings.ui.rsdarkmodesettingsystemsetting
msgid "Based on global system setting"
msgstr "Baseado na definição global do sistema"

#: spybot3.settings.ui.rsdarkmodesettingthreatlevel
msgid "Based on threat level"
msgstr "Baseado no nível de ameaça"

#: spybot3.settings.ui.rsproxymodecustomproxy
msgid "Use custom proxy"
msgstr "Utilizar proxy personalizado"

#: spybot3.settings.ui.rsproxymodenoproxy
msgid "Do not use a proxy"
msgstr "Não utilizar um ''proxy''"

#: spybot3.settings.ui.rsproxymodesystemproxy
msgid "Use system proxy"
msgstr "Utilizar ''proxy'' do sistema"

#: spybot3.settings.ui.rssettingsalwaysshowconnections
msgctxt "spybot3.settings.ui.rssettingsalwaysshowconnections"
msgid "Always show connections"
msgstr "Mostrar sempre as ligações"

#: spybot3.settings.ui.rssettingsdarkmode
msgid "Dark Mode"
msgstr "Modo Escuro"

#: spybot3.settings.ui.rssettingsdebugloglog
msgid "Show debug information"
msgstr "Mostrar informação de depuração"

#: spybot3.settings.ui.rssettingslanguagesystemdefault
msgid "System default language"
msgstr "Idioma predefinido do sistema"

#: spybot3.settings.ui.rssettingsproxymode
msgid "Proxy"
msgstr "Proxy"

#: spybot3.settings.ui.rssettingsshowmainmenu
msgid "Show assistive menu on Ctrl+M"
msgstr "Mostrar menu de assistência com Ctrl M"

#: spybot3.settings.ui.rssettingsshownotifications
msgid "Show notifications"
msgstr "Mostrar notificações"

#: spybot3.settings.ui.rssettingsusedyslexicfont
msgid "Improve text for dyslexic users"
msgstr "Melhorar o texto para os utilizadores disléxicos"

#: spybot3.settings.ui.rssettingsuserinterfacelanguage
msgid "User interface language"
msgstr "Idioma da interface do utilizador"

#: spybot3.settings.ui.rssettingsusespeechapi
msgid "Speak user interface elements"
msgstr "Falar elementos da interface do utilizador"

#: spybot3searchengines.database.rssearchengineenergystandard
msgid "bod standard"
msgstr "padrão do corpo"

#: spybot3searchengines.database.rssearchengineenergysustainable
msgid "sustainable"
msgstr "sustentável"

#: spybot3searchengines.database.rssearchengineprivacyfocus
msgid "privacy aware"
msgstr "ciente da privacidade"

#: spybot3searchengines.database.rssearchengineprivacystandard
msgid "standard"
msgstr "padrão"

#: spybot3searchengines.database.rssearchenginetypeinstalled
msgid "installed"
msgstr "instalado"

#: spybot3searchengines.database.rssearchenginetyperecommended
msgid "recommended"
msgstr "Recomendada"

#: tformspybot3antibeacon.caption
msgid "Spybot AntiBeacon"
msgstr "Spybot AntiBeacon"

