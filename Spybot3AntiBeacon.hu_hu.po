msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-01-12 19:41+0000\n"
"Last-Translator: Patrick Kolla <patrick.kolla@safer-networking.org>\n"
"Language-Team: Hungarian <http://translations.spybot.de/projects/spybot-anti-beacon/spybot-anti-beacon/hu/>\n"
"Language: hu_hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3"

#: antibeacon.branding.rsversiondescription
msgid "Stops telemetry of Windows, browsers, antivirus software and other apps."
msgstr "Leállítja a Windows, a böngészők, a víruskereső szoftverek és más alkalmazások telemetriáját."

#: antibeacon.controller.direct.rsantibeaconlicensebroken
msgid "Your license is broken, please re-install it!"
msgstr "Az Ön licencét elrontották, kérjük, telepítse újra!"

#: antibeacon.controller.direct.rsantibeaconlicenseexpired
msgid "Your license has expired!"
msgstr "Az engedélye lejárt!"

#: antibeacon.controller.direct.rsantibeaconlicensefree
msgid "Free version; buy Plus to get complete protection!"
msgstr "Ingyenes verzió; vásároljon Plus-t a teljes védelemhez!"

#: antibeacon.controller.direct.rsantibeaconlicenseinvalidproduct
#, object-pascal-format
msgid "Your license is meant for a different product (%0:s)!"
msgstr "Az Ön licencét egy másik termékre (%0:s) szánják!"

#: antibeacon.controller.direct.rsdisplaytextfirewalldefinition
msgid "Firewall: "
msgstr "Tűzfal: "

#: antibeacon.controller.direct.rsdisplaytextipdefinition
msgid "IPs: "
msgstr "IP-k: "

#: antibeacon.controller.direct.rsdisplaytextjsondefinition
msgid "JSON: "
msgstr "JSON: "

#: antibeacon.controller.direct.rsdisplaytextkeyvaluepair
msgid "KeyValuePair: "
msgstr "KeyValuePair: "

#: antibeacon.controller.direct.rsdisplaytextmozillaconfig
msgid "Mozilla Config: "
msgstr "Mozilla Config: "

#: antibeacon.controller.direct.rsdisplaytextregistrydefinition
msgid "Registry: "
msgstr "Nyilvántartás: "

#: antibeacon.controller.direct.rsdisplaytextservicedefinitionsimple
msgid "Service: "
msgstr "Szolgáltatás: "

#: antibeacon.controller.direct.rsdisplaytextstaskdefinition
msgid "Task: "
msgstr "Feladat: "

#: antibeacon.controller.direct.rstelemetrymonitoractive
msgctxt "antibeacon.controller.direct.rstelemetrymonitoractive"
msgid "Live Monitor active"
msgstr "Élő monitor aktív"

#: antibeacon.controller.direct.rstelemetrymonitornotinstalled
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotinstalled"
msgid "Live Monitor not installed"
msgstr "Live Monitor nincs telepítve"

#: antibeacon.controller.direct.rstelemetrymonitornotrunning
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotrunning"
msgid "Live Monitor not running"
msgstr "A Live Monitor nem fut"

#: antibeacon.controller.direct.rstelemetrymonitornotworking
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotworking"
msgid "Live Monitor failing"
msgstr "Élő monitor hibája"

#: antibeacon.controller.direct.rstelemetrymonitorpending
msgctxt "antibeacon.controller.direct.rstelemetrymonitorpending"
msgid "Live Monitor change pending..."
msgstr "Live Monitor változás folyamatban..."

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname
msgid "Refresh Anti-Beacon immunization"
msgstr "Frissítse fel a Beacon elleni immunizálást"

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname2
#, object-pascal-format
msgid "Refresh %s immunization"
msgstr "Frissítés %s immunizáció"

#: antibeacon.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will refresh your immunization against telemetry and tracking."
msgstr "Ez a feladat felfrissíti a telemetria és a nyomkövetés elleni immunizációt."

#: antibeacon.ui.form.rslivemonitorblockedcount
#, object-pascal-format
msgid "%d of %d blocked."
msgstr "%d-ből %d blokkolva."

#: antibeacon.ui.form.rslivemonitornothingmonitoredyet
msgid "Nothing monitored yet."
msgstr "Még nem figyeltek meg semmit."

#: antibeacon.ui.form.rsmenubarmonitor
msgctxt "antibeacon.ui.form.rsmenubarmonitor"
msgid "Live Monitor"
msgstr "Élő monitor"

#: antibeacon.ui.form.rsmenubarrecalldata
msgid "Windows Recall"
msgstr "Windows visszahívás"

#: antibeacon.ui.form.rsmenubarsearchengines
msgid "Search Providers"
msgstr "Keresési szolgáltatók"

#: antibeacon.ui.form.rsspybot2licenseheader
msgid "Replace expired or broken license"
msgstr "Lejárt vagy törött licenc cseréje"

#: antibeacon.ui.form.rsspybot2licenseprompt1
msgid "Your license is expired or broken. Do you want to replace it with this compatible one?"
msgstr "Az Ön engedélye lejárt vagy megszakadt. Szeretné kicserélni erre a kompatibilisre?"

#: antibeacon.ui.form.rsspybot2licenseprompt2
#, object-pascal-format
msgid "%0:s license for %1:s, valid until %2:s?"
msgstr "%0:s engedély %1:s-re, %2:s-ig érvényes?"

#: antibeacon.ui.form.rsspybot2licenseremember
msgid "Remember to ignore this license if I click \"No\"."
msgstr "Ne felejtse el figyelmen kívül hagyni ezt az engedélyt, ha a \"Nem\" gombra kattintok."

#: antibeacon.ui.form.rsstatusbuttonmonitor
msgid "Live Monitor initializing..."
msgstr "Live Monitor inicializálása..."

#: antibeacon.ui.frame.carbonfootprint.rsframecaption
msgid "About the Carbon Footprint of Telemetry"
msgstr "A telemetria szénlábnyomáról"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "engedély"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon"
msgid "block"
msgstr "blokk"

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "A leíró adatbázisban ellentmondásos adatok vannak erről a bejegyzésről. Kérjük, telepítse újra, vagy használja a fenti <i>Online</i> gombot."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "A leíró adatbázisban ellentmondásos adatok vannak erről a bejegyzésről. Kérjük, telepítse újra, vagy használja a fenti <i>Online</i> gombot."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloaded
msgid "The descriptions database has not been loaded. Please re-install or use the <i>Online</i> button above."
msgstr "A leírások adatbázisa nem lett betöltve. Kérjük, telepítse újra, vagy használja a fenti <i>Online</i> gombot."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloadednoonline
msgid "The descriptions database has not been loaded. Please re-install."
msgstr "A leírások adatbázisa nem lett betöltve. Kérjük, telepítse újra."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsimmunizernotfound
msgid "This immunizer is not yet described in the local description database. Please use the <i>Online</i> button above."
msgstr "Ez az immunizáló még nem szerepel a helyi leíró adatbázisban. Kérjük, használja a fenti <i>Online</i> gombot."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline"
msgid "Online"
msgstr "Online"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsdescription
msgid "Description"
msgstr "Leírás"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsentries
msgid "Entry Information (for the technically inclined user)"
msgstr "Belépési információk (a műszaki beállítottságú felhasználók számára)"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsframename
msgid "Immunizer Details"
msgstr "Immunizáló adatai"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisfree
msgid "This protection is available in the free and paid version of Anti-Beacon."
msgstr "Ez a védelem az Anti-Beacon ingyenes és fizetős változatában is elérhető."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisplus
msgid "This protection is only available in the paid version Spybot Anti-Beacon Plus."
msgstr "Ez a védelem csak a Spybot Anti-Beacon Plus fizetős változatában érhető el."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevel
msgid "Recommendation Level"
msgstr "Ajánlási szint"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways"
msgid "Always"
msgstr "Mindig"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelmaximal
msgid "Maximum Protection"
msgstr "Maximális védelem"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelminimal
msgid "Minimal Protection"
msgstr "Minimális védelem"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnever
msgid "Not Recommended"
msgstr "Nem ajánlott"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone"
msgid "None"
msgstr "Nincs"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelrecommended
msgid "Recommended Protection"
msgstr "Ajánlott védelem"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus"
msgid "Status"
msgstr "Állapot"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Ez az immunizáló csak a Spybot Anti-Beacon Plusban érhető el."

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "engedély"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon"
msgid "block"
msgstr "blokk"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroup3p
msgid "Third Party Analytics"
msgstr "Harmadik fél analitika"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupapps
msgid "Apps"
msgstr "Alkalmazások"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupav
msgid "Antivirus"
msgstr "Antivirus"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupbrowser
msgid "Browser"
msgstr "Böngésző"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupgames
msgid "Games"
msgstr "Játékok"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongrouphwdrives
msgid "Hardware Drivers"
msgstr "Hardveres meghajtók"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoem
msgid "Preinstalled Manufacturer Software"
msgstr "Előre telepített gyártói szoftver"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoffice
msgid "Office Software"
msgstr "Irodai szoftver"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupos
msgid "Operating System"
msgstr "Operációs rendszer"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionapps
msgid "App Telemetry"
msgstr "Alkalmazás telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionappsinfree
msgid "App Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "App Telemetria - vásárolja meg a Spybot Anti-Beacon Plus-t, hogy megkapja ezeket!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsers
msgid "Browser Telemetry"
msgstr "Böngésző telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree"
msgid "Browser Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Böngésző-telemetria - vásárolja meg a Spybot Anti-Beacon Plus-t, hogy megkapja ezeket!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptioncustominfree
#, object-pascal-format
msgid "%s - buy Spybot Anti-Beacon Plus to get these!"
msgstr "%s - vásárolja meg a Spybot Anti-Beacon Plus-t, hogy megkapja ezeket!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionmain
msgid "Microsoft Windows Telemetry"
msgstr "Microsoft Windows Telemetry"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplus
msgid "More Telemetry"
msgstr "További telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplusinfree
msgid "More Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "További telemetria - vásárolja meg a Spybot Anti-Beacon Plus-t, hogy megkapja ezeket!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalled
msgid "Pre-Installed Manufacturer Telemetry"
msgstr "Előre telepített gyártói telemetria"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalledinfree
msgid "Pre-Installed Manufacturer Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Előre telepített gyártói telemetria - vásárolja meg a Spybot Anti-Beacon Plus-t, hogy megkapja ezeket!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply"
msgid "Apply"
msgstr "Alkalmazás"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactioncancel
msgid "Cancel"
msgstr "Törölje"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionreset
msgid "Reset"
msgstr "Reset"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersavailable
msgid "immunizers available"
msgstr "rendelkezésre álló immunizálószerek"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersframecaption
msgid "Telemetry options"
msgstr "Telemetriai lehetőségek"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerslevel
msgid "Protection Presets ↓"
msgstr "Védelmi előbeállítások ↓"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways"
msgid "Always"
msgstr "Mindig"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectmaximal
msgid "Full"
msgstr "Teljes"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectminimal
msgid "Minimal"
msgstr "Minimális"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnever
msgid "Never"
msgstr "Soha"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone"
msgid "None"
msgstr "Nincs"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectrecommended
msgid "Recommended"
msgstr "Ajánlott"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerunavailable
msgid "not found on your system"
msgstr "nem található a rendszerében"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Ez az immunizáló csak a Spybot Anti-Beacon Plusban érhető el."

#: antibeacon.ui.frame.livemonitor.rsablivemonitorcarboninfo
msgid "Carbon..."
msgstr "Szén..."

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespan
msgid "Display Range ↓"
msgstr "Megjelenítési tartomány ↓"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespansession
msgid "Current Session (since system start)"
msgstr "Jelenlegi munkamenet (a rendszer indítása óta)"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespantotal
msgid "Total (since installation)"
msgstr "Összesen (a telepítés óta)"

#: antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption
msgctxt "antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption"
msgid "Live Monitor"
msgstr "Élő monitor"

#: antibeacon.ui.frame.livemonitor.rslivemonitorservicetableentry
msgid "Live Monitor,,"
msgstr "Live Monitor,,"

#: antibeacon.ui.frame.livemonitor.rslivemonitortableconfiguration
msgid "Configuration"
msgstr "Konfiguráció"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitoractiveprompt
msgid "Telemetry Monitor is working fine; do you want to stop it anyway?"
msgstr "A Telemetria Monitor jól működik; mégis le akarja állítani?"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotinstalledaction
msgid "click to install"
msgstr "kattintson a telepítéshez"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotrunningaction
msgid "click to start"
msgstr "kattintson az indításhoz"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotworkingaction
msgid "click to restart"
msgstr "kattintson az újraindításhoz"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitlesession
msgid "telemetry connection attempts seen since started"
msgstr "a telemetriakapcsolati kísérletek az indulás óta"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitletotal
msgid "telemetry connection attempts seen ever"
msgstr "valaha látott telemetriai csatlakozási kísérletek"

#: antibeacon.ui.frame.main.rsantibeaconcount
msgid "Blocked telemetry options"
msgstr "Blokkolt telemetriás opciók"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4
#, object-pascal-format
msgid "%0:d out of %1:d protected"
msgstr "%0:d out of %1:d védett"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4unlicensed
#, object-pascal-format
msgid "%0:d out of %1:d protected - %2:d require %3:s Plus"
msgstr "%0:d out of %1:d védett - %2:d igényli %3:s Plusz"

#: antibeacon.ui.frame.main.rsantibeaconcustomize
msgid "Customize"
msgstr "Testreszabás"

#: antibeacon.ui.frame.main.rsantibeaconlastaction
msgid "Last action"
msgstr "Utolsó intézkedés"

#: antibeacon.ui.frame.main.rsantibeaconlicense
msgctxt "antibeacon.ui.frame.main.rsantibeaconlicense"
msgid "License"
msgstr "Licenc"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonbuy
msgid "click here to buy a license"
msgstr "Lejárt vagy törött licenc cseréje"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonrenew
msgid "click to renew your License"
msgstr "kattintson a licenc megújításához"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuy
msgid "License (click here to buy)"
msgstr "Licenc (kattintson ide a vásárláshoz)"

#: antibeacon.ui.frame.main.rsantibeaconlicenserenew
msgid "License (click here to renew)"
msgstr "Licenc (kattintson ide a megújításhoz)"

#: antibeacon.ui.frame.main.rsantibeaconlive
msgctxt "antibeacon.ui.frame.main.rsantibeaconlive"
msgid "Live Monitor"
msgstr "Élő monitor"

#: antibeacon.ui.frame.main.rsantibeaconmissinglibraries
#, object-pascal-format
msgid "%0:s missing"
msgstr "%0:s hiányzik"

#: antibeacon.ui.frame.main.rsantibeaconprotecedmorecount
#, object-pascal-format, badformat
msgid "+ %0:d protected!"
msgstr "+ %0:d védett"

#: antibeacon.ui.frame.main.rsantibeaconprotect
msgid "Protect"
msgstr "Védje"

#: antibeacon.ui.frame.main.rsantibeacontoasttitle
msgid "Protection Status"
msgstr "Védelmi státusz"

#: antibeacon.ui.frame.main.rsantibeacontranslationerror
msgid "There might be an issue with your translation. Please check for program updates."
msgstr "Lehet, hogy a fordítással van valami probléma. Kérjük, ellenőrizze a program frissítéseit."

#: antibeacon.ui.frame.main.rsantibeaconunprotect
msgid "Unprotect"
msgstr "Védelem feloldása"

#: antibeacon.ui.settings.rsantibeaconsettingsdownloadevaluationlicense
msgid "Check for available evaluation license on program start"
msgstr "A program indításakor ellenőrizze a rendelkezésre álló értékelő licenc meglétét"

#: antibeacon.ui.settings.rsantibeaconsettingsscheduledtaskcaption
msgid "Re-Immunize at each Logon (via Scheduled Task)"
msgstr "Újra-immunizálás minden bejelentkezéskor (ütemezett feladattal)"

#: antibeacon.ui.settings.rsantibeaconsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Frissítések keresése a program indításakor"

#: antirecall.ui.framedata.rsrecalldataactiondeletescreenshots
msgid "Delete screenshots"
msgstr "Képernyőképek törlése"

#: antirecall.ui.framedata.rsrecalldatacountscreenshot
msgid "Recorded screenshots found"
msgstr "Rögzített képernyőképeket találtak"

#: antirecall.ui.framedata.rsrecalldatacounttextextracts
msgid "Extracted text snippets found"
msgstr "Kinyert szövegrészletek találhatók"

#: antirecall.ui.framedata.rsrecalldatadbfound
msgid "Recall database(s) detected"
msgstr "Észlelt adatbázis(ok) visszahívása"

#: antirecall.ui.framedata.rsrecalldatadetectedcount
msgid "data fragments within Recall detected"
msgstr "adattöredékek észlelhetők a Visszahíváson belül"

#: antirecall.ui.framedata.rsrecalldataframecaption
msgid "Microsoft Recall Data Overview"
msgstr "A Microsoft visszahívási adatainak áttekintése"

#: antirecall.ui.framedata.rsrecalldatagrouppoliysystem
msgid "Windows AI Status (for complete system)"
msgstr "Windows AI állapot (a teljes rendszerhez)"

#: antirecall.ui.framedata.rsrecalldatagrouppoliyuser
msgid "Windows AI Status (for current user)"
msgstr "Windows AI állapot (az aktuális felhasználó számára)"

#: antirecall.ui.framedata.rsrecalldataloading
msgid "Analyzing Windows Recall data..."
msgstr "A Windows visszahívási adatainak elemzése..."

#: antirecall.ui.framedata.rsrecalldataloadingshort
msgctxt "antirecall.ui.framedata.rsrecalldataloadingshort"
msgid "Loading..."
msgstr "Berakás..."

#: antirecall.ui.framedata.rsrecalldataprofilescount
msgid "Recall profiles detected"
msgstr "Visszahívási profilok észlelése"

#: antirecall.ui.framedata.rsrecalldatasettingdefault
msgid "Default"
msgstr "Alapértelmezett"

#: antirecall.ui.framedata.rsrecalldatasettingdisabled
msgid "Disabled"
msgstr "Fogyatékos"

#: antirecall.ui.framedata.rsrecalldatasettingenabled
msgid "Default (Enabled)"
msgstr "Alapértelmezett (engedélyezve)"

#: antirecall.ui.framedata.rsrecalldatasettingunknown
msgid "Unknown"
msgstr "Ismeretlen"

#: antirecall.ui.framedata.rsrecalldeletescreenshotsdialogmessage
#, object-pascal-format
msgid "Deleted %:0d of %:1d found Recall screenshots."
msgstr "Törölve %:0d a(z) %:1d közül talált Képernyőképek visszahívása."

#: antirecall.ui.framedata.rsrecalltablecategoryprofiles
msgid "Profiles"
msgstr "Profilok"

#: antirecall.ui.framedata.rsrecalltableentryprofiledetailsscreenshots
#, object-pascal-format
msgid "%d screenshots"
msgstr "%d képernyőképek"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitor
msgid "Spybot Identity Monitor Preview"
msgstr "A Spybot Identity Monitor előzetes verziója"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorchecking
msgid "Testing for breaches..."
msgstr "Szabálysértések tesztelése..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorpending
msgid "..."
msgstr "..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitle
#, object-pascal-format
msgid "%0:d breaches found"
msgstr "%0:D szabálysértések találhatók"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitlenone
msgid "No breaches found"
msgstr "Nem találtak jogsértést"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableclean
msgid "No breaches found. Use Identity Monitor to regularly check this and more accounts."
msgstr "Nem találtak jogsértést. Az Identity Monitor használatával rendszeresen ellenőrizheti ezt és további fiókokat."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortablefound
msgid "Breaches found! Use Identity Monitor for details."
msgstr "Szabálysértések találhatók! A részletekért használja az Identitásfigyelőt."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableusercaption
msgid "Monitored user (in Identity Monitor, you can set up more)"
msgstr "Figyelt felhasználó (az Identitásfigyelőben többet is beállíthat)"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelclean
msgid "Logon clean"
msgstr "Bejelentkezés tiszta"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfail
msgid "Failed"
msgstr "Sikertelen"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfound
msgid "Breaches found!"
msgstr "Szabálysértések találhatók!"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheeltesting
msgid "Testing"
msgstr "Vizsgálat"

#: spybot3.settings.ui.rsdarkmodesettingalwaysoff
msgid "Always off"
msgstr "Mindig ki van kapcsolva"

#: spybot3.settings.ui.rsdarkmodesettingalwayson
msgid "Always on"
msgstr "Mindig be van kapcsolva"

#: spybot3.settings.ui.rsdarkmodesettingnighttime
msgid "On during night time"
msgstr "Éjszaka bekapcsolva"

#: spybot3.settings.ui.rsdarkmodesettingsystemsetting
msgid "Based on global system setting"
msgstr "A globális rendszerbeállítás alapján"

#: spybot3.settings.ui.rsdarkmodesettingthreatlevel
msgid "Based on threat level"
msgstr "A fenyegetettségi szint alapján"

#: spybot3.settings.ui.rsproxymodecustomproxy
msgid "Use custom proxy"
msgstr "Egyéni proxy használata"

#: spybot3.settings.ui.rsproxymodenoproxy
msgid "Do not use a proxy"
msgstr "Ne használjon proxy-t"

#: spybot3.settings.ui.rsproxymodesystemproxy
msgid "Use system proxy"
msgstr "Rendszerproxy használata"

#: spybot3.settings.ui.rssettingsalwaysshowconnections
msgctxt "spybot3.settings.ui.rssettingsalwaysshowconnections"
msgid "Always show connections"
msgstr "Mindig mutassa a kapcsolatokat"

#: spybot3.settings.ui.rssettingsdarkmode
msgid "Dark Mode"
msgstr "Sötét üzemmód"

#: spybot3.settings.ui.rssettingsdebugloglog
msgid "Show debug information"
msgstr "Hibakeresési információk megjelenítése"

#: spybot3.settings.ui.rssettingslanguagesystemdefault
msgid "System default language"
msgstr "A rendszer alapértelmezett nyelve"

#: spybot3.settings.ui.rssettingsproxymode
msgid "Proxy"
msgstr "Proxy"

#: spybot3.settings.ui.rssettingsshowmainmenu
msgid "Show assistive menu on Ctrl+M"
msgstr "Segédmenü megjelenítése a Ctrl M parancsra"

#: spybot3.settings.ui.rssettingsshownotifications
msgid "Show notifications"
msgstr "Értesítések megjelenítése"

#: spybot3.settings.ui.rssettingsusedyslexicfont
msgid "Improve text for dyslexic users"
msgstr "A szöveg javítása a diszlexiás felhasználók számára"

#: spybot3.settings.ui.rssettingsuserinterfacelanguage
msgid "User interface language"
msgstr "A felhasználói felület nyelve"

#: spybot3.settings.ui.rssettingsusespeechapi
msgid "Speak user interface elements"
msgstr "Beszéljen a felhasználói felület elemeiről"

#: spybot3searchengines.database.rssearchengineenergystandard
msgid "bod standard"
msgstr "BOD szabvány"

#: spybot3searchengines.database.rssearchengineenergysustainable
msgid "sustainable"
msgstr "fenntartható"

#: spybot3searchengines.database.rssearchengineprivacyfocus
msgid "privacy aware"
msgstr "Adatvédelmi tudatosság"

#: spybot3searchengines.database.rssearchengineprivacystandard
msgid "standard"
msgstr "szabvány"

#: spybot3searchengines.database.rssearchenginetypeinstalled
msgid "installed"
msgstr "Telepített"

#: spybot3searchengines.database.rssearchenginetyperecommended
msgid "recommended"
msgstr "Ajánlott"

#: tformspybot3antibeacon.caption
msgid "Spybot AntiBeacon"
msgstr "Spybot AntiBeacon"

