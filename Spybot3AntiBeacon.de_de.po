msgid ""
msgstr ""
"Project-Id-Version: Spybot Anti-Beacon\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-06-14 10:17+0200\n"
"Last-Translator: Patrick Kolla <patrick.kolla@safer-networking.org>\n"
"Language-Team: German <http://translations.spybot.de/projects/spybot-anti-beacon/spybot-anti-beacon/de/>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.4.4\n"

#: antibeacon.branding.rsversiondescription
msgid "Stops telemetry of Windows, browsers, antivirus software and other apps."
msgstr "Stoppt die Telemetrie von Windows, Browsern, Antivirus-Software und anderen Anwendungen."

#: antibeacon.controller.direct.rsantibeaconlicensebroken
msgid "Your license is broken, please re-install it!"
msgstr "Ihre Lizenz ist beschädigt bitte installieren Sie sie erneut!"

#: antibeacon.controller.direct.rsantibeaconlicenseexpired
msgid "Your license has expired!"
msgstr "Ihre Lizenz ist abgelaufen!"

#: antibeacon.controller.direct.rsantibeaconlicensefree
msgid "Free version; buy Plus to get complete protection!"
msgstr "Kostenlose Version; kaufen Sie Plus für vollständigen Schutz!"

#: antibeacon.controller.direct.rsantibeaconlicenseinvalidproduct
#, object-pascal-format
msgid "Your license is meant for a different product (%0:s)!"
msgstr "Diese Lizenz ist für ein anderes Produkt gedacht (%0:s)!"

#: antibeacon.controller.direct.rsdisplaytextfirewalldefinition
msgid "Firewall: "
msgstr "Firewalll: "

#: antibeacon.controller.direct.rsdisplaytextipdefinition
msgid "IPs: "
msgstr "IPs: "

#: antibeacon.controller.direct.rsdisplaytextjsondefinition
msgid "JSON: "
msgstr "JSON: "

#: antibeacon.controller.direct.rsdisplaytextkeyvaluepair
msgid "KeyValuePair: "
msgstr "Schlüssel-Wert-Paar: "

#: antibeacon.controller.direct.rsdisplaytextmozillaconfig
msgid "Mozilla Config: "
msgstr "Mozilla-Konfiguration: "

#: antibeacon.controller.direct.rsdisplaytextregistrydefinition
msgid "Registry: "
msgstr "Registrierungsdatenbank: "

#: antibeacon.controller.direct.rsdisplaytextservicedefinitionsimple
msgid "Service: "
msgstr "Systemdienst: "

#: antibeacon.controller.direct.rsdisplaytextstaskdefinition
msgid "Task: "
msgstr "Geplante Aufgabe: "

#: antibeacon.controller.direct.rstelemetrymonitoractive
msgctxt "antibeacon.controller.direct.rstelemetrymonitoractive"
msgid "Live Monitor active"
msgstr "Live-Monitor aktiv"

#: antibeacon.controller.direct.rstelemetrymonitornotinstalled
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotinstalled"
msgid "Live Monitor not installed"
msgstr "Live-Monitor nicht installiert"

#: antibeacon.controller.direct.rstelemetrymonitornotrunning
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotrunning"
msgid "Live Monitor not running"
msgstr "Live-Monitor läuft nicht"

#: antibeacon.controller.direct.rstelemetrymonitornotworking
msgctxt "antibeacon.controller.direct.rstelemetrymonitornotworking"
msgid "Live Monitor failing"
msgstr "Live-Monitor ausgefallen"

#: antibeacon.controller.direct.rstelemetrymonitorpending
msgctxt "antibeacon.controller.direct.rstelemetrymonitorpending"
msgid "Live Monitor change pending..."
msgstr "Live-Monitor Statusänderung..."

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname
msgid "Refresh Anti-Beacon immunization"
msgstr "Auffrischen der Anti-Beacon-Immunisierung"

#: antibeacon.scheduledtask.rsscheduledtaskdisplayname2
#, object-pascal-format
msgid "Refresh %s immunization"
msgstr "Auffrischen der %s-Immunisierung"

#: antibeacon.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will refresh your immunization against telemetry and tracking."
msgstr "Diese Aufgabe wird Ihre Immunisierung gegen Telemetrie auffrischen."

#: antibeacon.ui.form.rslivemonitorblockedcount
#, object-pascal-format
msgid "%d of %d blocked."
msgstr "%d von %d blockiert."

#: antibeacon.ui.form.rslivemonitornothingmonitoredyet
msgid "Nothing monitored yet."
msgstr "Bisher wurde nichts überwacht."

#: antibeacon.ui.form.rsmenubarmonitor
msgctxt "antibeacon.ui.form.rsmenubarmonitor"
msgid "Live Monitor"
msgstr "Live-Monitor"

#: antibeacon.ui.form.rsmenubarrecalldata
msgid "Windows Recall"
msgstr "Windows Recall"

#: antibeacon.ui.form.rsmenubarsearchengines
msgid "Search Providers"
msgstr "Suche nach Anbietern"

#: antibeacon.ui.form.rsspybot2licenseheader
msgid "Replace expired or broken license"
msgstr "Abgelaufene oder beschädigte Lizenz ersetzen"

#: antibeacon.ui.form.rsspybot2licenseprompt1
msgid "Your license is expired or broken. Do you want to replace it with this compatible one?"
msgstr "Ihre Lizenz ist abgelaufen oder beschädigt. Wollen Sie sie mit dieser kompatiblen Lizenz ersetzen?"

#: antibeacon.ui.form.rsspybot2licenseprompt2
#, object-pascal-format
msgid "%0:s license for %1:s, valid until %2:s?"
msgstr "%0:s Lizenz für %1:s, gültig bis %2:s?"

#: antibeacon.ui.form.rsspybot2licenseremember
msgid "Remember to ignore this license if I click \"No\"."
msgstr "Merken, dass ich diese Lizenz ignorieren will, wenn ich \"Nein\" klicke."

#: antibeacon.ui.form.rsstatusbuttonmonitor
msgid "Live Monitor initializing..."
msgstr "Live-Monitor initialisiert..."

#: antibeacon.ui.frame.carbonfootprint.rsframecaption
msgid "About the Carbon Footprint of Telemetry"
msgstr "Über das von Telemetrie verursachte CO2"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "erlauben"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconcheckboxon"
msgid "block"
msgstr "blockieren"

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionbroken"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "Die Beschreibungsdatenbank hat inkonsistente Daten für diesen Eintrag. Bitte installieren Sie neu oder verwenden den <i>Online</i>-Knopf oben."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsdescriptionunknown"
msgid "The description database has inconsistent data on this entry. Please re-install or use the <i>Online</i> button above."
msgstr "Die Beschreibungsdatenbank hat inkonsistente Daten für diesen Eintrag. Bitte installieren Sie neu oder verwenden den <i>Online</i>-Knopf oben."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloaded
msgid "The descriptions database has not been loaded. Please re-install or use the <i>Online</i> button above."
msgstr "Die Beschreibungsdatenbank wurde nicht geladen. Bitte installieren Sie neu oder verwenden den <i>Online</i>-Knopf oben."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsfilenotloadednoonline
msgid "The descriptions database has not been loaded. Please re-install."
msgstr "Die Beschreibungsdatenbank wurde nicht geladen."

#: antibeacon.ui.frame.immunizers.details.rsantibeacondescriptionsimmunizernotfound
msgid "This immunizer is not yet described in the local description database. Please use the <i>Online</i> button above."
msgstr "Diese Einstellung hat noch keinen Eintrag in der Beschreibungsdatenbank. Bitte verwenden den <i>Online</i>-Knopf oben."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsactiononline"
msgid "Online"
msgstr "Online"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsdescription
msgid "Description"
msgstr "Beschreibung"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsentries
msgid "Entry Information (for the technically inclined user)"
msgstr "Einträge (für den technisch versierten Benutzer)"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsframename
msgid "Immunizer Details"
msgstr "Immunisierer-Details"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisfree
msgid "This protection is available in the free and paid version of Anti-Beacon."
msgstr "Dieser Schutz ist sowohl in der kostenlosen als auch bezahlten Version von Anti-Beacon enthalten."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsisplus
msgid "This protection is only available in the paid version Spybot Anti-Beacon Plus."
msgstr "Dieser Schutz ist nur in der bezahlten Version Spybot Anti-Beacon Plus enthalten."

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevel
msgid "Recommendation Level"
msgstr "Empfehlungen"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelalways"
msgid "Always"
msgstr "Immer"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelmaximal
msgid "Maximum Protection"
msgstr "Maximaler Schutz"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelminimal
msgid "Minimal Protection"
msgstr "Minimaler Schutz"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnever
msgid "Not Recommended"
msgstr "Nicht empfohlen"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelnone"
msgid "None"
msgstr "Keine"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailslevelrecommended
msgid "Recommended Protection"
msgstr "Empfohlener Schutz"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerdetailsstatus"
msgid "Status"
msgstr "Status"

#: antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.details.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Dieser Immunisierer ist nur in Spybot Anti-Beacon Plus enthalten."

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxoff"
msgid "permit"
msgstr "erlauben"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconcheckboxon"
msgid "block"
msgstr "blockieren"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroup3p
msgid "Third Party Analytics"
msgstr "Analyse durch Drittanbieter"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupapps
msgid "Apps"
msgstr "Anwendungen"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupav
msgid "Antivirus"
msgstr "Antivirensoftware"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupbrowser
msgid "Browser"
msgstr "Browser"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupgames
msgid "Games"
msgstr "Spiele"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongrouphwdrives
msgid "Hardware Drivers"
msgstr "Geräte-Treiber"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoem
msgid "Preinstalled Manufacturer Software"
msgstr "Vorinstallierte Software"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupoffice
msgid "Office Software"
msgstr "Büro-Software"

#: antibeacon.ui.frame.immunizers.list.rsantibeacongroupos
msgid "Operating System"
msgstr "Betriebssystem"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionapps
msgid "App Telemetry"
msgstr "Anwendungs-Telemetrie"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionappsinfree
msgid "App Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Anwendungs-Telemetrie - bekommen Sie beim Kauf von Spybot Anti-Beacon Plus!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsers
msgid "Browser Telemetry"
msgstr "Browser-Telemetrie"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionbrowsersinfree"
msgid "Browser Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Browser-Telemetrie - bekommen Sie beim Kauf von Spybot Anti-Beacon Plus!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptioncustominfree
#, object-pascal-format
msgid "%s - buy Spybot Anti-Beacon Plus to get these!"
msgstr "%s - bekommen Sie beim Kauf von Spybot Anti-Beacon Plus!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionmain
msgid "Microsoft Windows Telemetry"
msgstr "Standard-Immunisierer"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplus
msgid "More Telemetry"
msgstr "Mehr Telemetrie"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionplusinfree
msgid "More Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Mehr Telemetrie - bekommen Sie beim Kauf von Spybot Anti-Beacon Plus!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalled
msgid "Pre-Installed Manufacturer Telemetry"
msgstr "Telemetrie von vorinstallierter Herstellersoftware"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizergroupcaptionpreinstalledinfree
msgid "Pre-Installed Manufacturer Telemetry - buy Spybot Anti-Beacon Plus to get these!"
msgstr "Telemetrie von vorinstallierter Herstellersoftware - benötigt Anti-Beacon Plus!"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionapply"
msgid "Apply"
msgstr "Anwenden"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactioncancel
msgid "Cancel"
msgstr "Abbrechen"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersactionreset
msgid "Reset"
msgstr "Zurücksetzen"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersavailable
msgid "immunizers available"
msgstr "Immunisierer verfügbar"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersframecaption
msgid "Telemetry options"
msgstr "Telemetrie-Optionen"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerslevel
msgid "Protection Presets ↓"
msgstr "Empfehlungen ↓"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectalways"
msgid "Always"
msgstr "Immer"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectmaximal
msgid "Full"
msgstr "Vollständig"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectminimal
msgid "Minimal"
msgstr "Minimal"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnever
msgid "Never"
msgstr "Nie"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectnone"
msgid "None"
msgstr "Keine"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizersselectrecommended
msgid "Recommended"
msgstr "Empfohlen"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerunavailable
msgid "not found on your system"
msgstr "nicht auf Ihrem System"

#: antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly
msgctxt "antibeacon.ui.frame.immunizers.list.rsantibeaconimmunizerwarningproonly"
msgid "This immunizer is only available in Spybot Anti-Beacon Plus."
msgstr "Dieser Immunisierer ist nur in Spybot Anti-Beacon Plus enthalten."

#: antibeacon.ui.frame.livemonitor.rsablivemonitorcarboninfo
msgid "Carbon..."
msgstr "CO2..."

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespan
msgid "Display Range ↓"
msgstr "Darstellungsbereich ↓"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespansession
msgid "Current Session (since system start)"
msgstr "Aktuelle Sitzung (seit Systemstart)"

#: antibeacon.ui.frame.livemonitor.rsablivemonitortimespantotal
msgid "Total (since installation)"
msgstr "Insgesamt (seit Installation)"

#: antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption
msgctxt "antibeacon.ui.frame.livemonitor.rsantibeaconlivemonitorframecaption"
msgid "Live Monitor"
msgstr "Live-Monitor"

#: antibeacon.ui.frame.livemonitor.rslivemonitorservicetableentry
msgid "Live Monitor,,"
msgstr "Live-Monitor,,"

#: antibeacon.ui.frame.livemonitor.rslivemonitortableconfiguration
msgid "Configuration"
msgstr "Konfiguration"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitoractiveprompt
msgid "Telemetry Monitor is working fine; do you want to stop it anyway?"
msgstr "Der Telemetriemonitor funktioniert einwandfrei; wollen Sie ihn trotzdem stoppen?"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotinstalledaction
msgid "click to install"
msgstr "zum Installieren anklicken"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotrunningaction
msgid "click to start"
msgstr "zum Starten anklicken"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitornotworkingaction
msgid "click to restart"
msgstr "zum Neustart anklicken"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitlesession
msgid "telemetry connection attempts seen since started"
msgstr "Telemetrie-Verbindungsversuche seit dem Start"

#: antibeacon.ui.frame.livemonitor.rstelemetrymonitortitletotal
msgid "telemetry connection attempts seen ever"
msgstr "Telemetrie-Verbindungsversuche, die jemals gesehen wurden"

#: antibeacon.ui.frame.main.rsantibeaconcount
msgid "Blocked telemetry options"
msgstr "Blockierte Telemetrie-Optionen"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4
#, object-pascal-format
msgid "%0:d out of %1:d protected"
msgstr "%0:d von %1:d geschützt"

#: antibeacon.ui.frame.main.rsantibeaconcountdata4unlicensed
#, object-pascal-format
msgid "%0:d out of %1:d protected - %2:d require %3:s Plus"
msgstr "%0:d aus %1:d geschützt - %2:d benötigen %3:s Plus"

#: antibeacon.ui.frame.main.rsantibeaconcustomize
msgid "Customize"
msgstr "Anpassen"

#: antibeacon.ui.frame.main.rsantibeaconlastaction
msgid "Last action"
msgstr "Letzte Aktion"

#: antibeacon.ui.frame.main.rsantibeaconlicense
msgctxt "antibeacon.ui.frame.main.rsantibeaconlicense"
msgid "License"
msgstr "Lizenz"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonbuy
msgid "click here to buy a license"
msgstr "Abgelaufene oder beschädigte Lizenz ersetzen"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuttonrenew
msgid "click to renew your License"
msgstr "Klicken Sie hier, um Ihre Lizenz zu verlängern"

#: antibeacon.ui.frame.main.rsantibeaconlicensebuy
msgid "License (click here to buy)"
msgstr "Lizenz (zum Kaufen hier klicken)"

#: antibeacon.ui.frame.main.rsantibeaconlicenserenew
msgid "License (click here to renew)"
msgstr "Lizenz (zum Verlängern hier klicken)"

#: antibeacon.ui.frame.main.rsantibeaconlive
msgctxt "antibeacon.ui.frame.main.rsantibeaconlive"
msgid "Live Monitor"
msgstr "Live-Monitor"

#: antibeacon.ui.frame.main.rsantibeaconmissinglibraries
#, object-pascal-format
msgid "%0:s missing"
msgstr "%0:s fehlt"

#: antibeacon.ui.frame.main.rsantibeaconprotecedmorecount
#, object-pascal-format
msgid "+ %0:d protected!"
msgstr "+ %0:d geschützt!"

#: antibeacon.ui.frame.main.rsantibeaconprotect
msgid "Protect"
msgstr "Schützen"

#: antibeacon.ui.frame.main.rsantibeacontoasttitle
msgid "Protection Status"
msgstr "Schutzstatus"

#: antibeacon.ui.frame.main.rsantibeacontranslationerror
msgid "There might be an issue with your translation. Please check for program updates."
msgstr "Es gibt ein Problem mit der Übersetzung. Bitte schauen Sie, ob es ein Programm-Update gibt."

#: antibeacon.ui.frame.main.rsantibeaconunprotect
msgid "Unprotect"
msgstr "Aufheben"

#: antibeacon.ui.settings.rsantibeaconsettingsdownloadevaluationlicense
msgid "Check for available evaluation license on program start"
msgstr "Prüfen Sie beim Programmstart, ob eine Testlizenz verfügbar ist"

#: antibeacon.ui.settings.rsantibeaconsettingsscheduledtaskcaption
msgid "Re-Immunize at each Logon (via Scheduled Task)"
msgstr "Beim Anmelden re-immunisieren"

#: antibeacon.ui.settings.rsantibeaconsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Bei Programmstart auf Updates prüfen"

#: antirecall.ui.framedata.rsrecalldataactiondeletescreenshots
msgid "Delete screenshots"
msgstr "Bildschirmphotos löschen"

#: antirecall.ui.framedata.rsrecalldatacountscreenshot
msgid "Recorded screenshots found"
msgstr "Aufgenommene Bildschirmfotos gefunden"

#: antirecall.ui.framedata.rsrecalldatacounttextextracts
msgid "Extracted text snippets found"
msgstr "Extrahierte Textfragmente gefunden"

#: antirecall.ui.framedata.rsrecalldatadbfound
msgid "Recall database(s) detected"
msgstr "Ermittelte Recall-Datenbanken"

#: antirecall.ui.framedata.rsrecalldatadetectedcount
msgid "data fragments within Recall detected"
msgstr "Datenfragmente innerhalb von Recall entdeckt"

#: antirecall.ui.framedata.rsrecalldataframecaption
msgid "Microsoft Recall Data Overview"
msgstr "Microsoft Recall Daten-Übersicht"

#: antirecall.ui.framedata.rsrecalldatagrouppoliysystem
msgid "Windows AI Status (for complete system)"
msgstr "Windows AI-Status (für das gesamte System)"

#: antirecall.ui.framedata.rsrecalldatagrouppoliyuser
msgid "Windows AI Status (for current user)"
msgstr "Windows AI-Status (für den aktuellen Benutzer)"

#: antirecall.ui.framedata.rsrecalldataloading
msgid "Analyzing Windows Recall data..."
msgstr "Analysieren von Windows Recall-Daten..."

#: antirecall.ui.framedata.rsrecalldataloadingshort
msgctxt "antirecall.ui.framedata.rsrecalldataloadingshort"
msgid "Loading..."
msgstr "Laden..."

#: antirecall.ui.framedata.rsrecalldataprofilescount
msgid "Recall profiles detected"
msgstr "Ermittelte Recall-Profile"

#: antirecall.ui.framedata.rsrecalldatasettingdefault
msgid "Default"
msgstr "Standard"

#: antirecall.ui.framedata.rsrecalldatasettingdisabled
msgid "Disabled"
msgstr "Deaktiviert"

#: antirecall.ui.framedata.rsrecalldatasettingenabled
msgid "Default (Enabled)"
msgstr "Standard (Aktiviert)"

#: antirecall.ui.framedata.rsrecalldatasettingunknown
msgid "Unknown"
msgstr "Unbekannt"

#: antirecall.ui.framedata.rsrecalldeletescreenshotsdialogmessage
#, object-pascal-format
msgid "Deleted %:0d of %:1d found Recall screenshots."
msgstr "%:0d von %:1d Recall-Bildschirmfotos gefunden."

#: antirecall.ui.framedata.rsrecalltablecategoryprofiles
msgid "Profiles"
msgstr "Profile"

#: antirecall.ui.framedata.rsrecalltableentryprofiledetailsscreenshots
#, object-pascal-format
msgid "%d screenshots"
msgstr "%d Bildschirmphotos"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitor
msgid "Spybot Identity Monitor Preview"
msgstr "Spybot Identity Monitor Vorschau"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorbuttonbarname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorchecking
msgid "Testing for breaches..."
msgstr "Prüfung auf Verstöße..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorpending
msgid "..."
msgstr "..."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitle
#, object-pascal-format
msgid "%0:d breaches found"
msgstr "%0:d Verstöße gefunden"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorstatusbuttonsubtitlenone
msgid "No breaches found"
msgstr "Keine Verstöße gefunden"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableclean
msgid "No breaches found. Use Identity Monitor to regularly check this and more accounts."
msgstr "Keine Verstöße gefunden. Verwenden Sie Identity Monitor, um dieses und weitere Konten regelmäßig zu überprüfen."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortablefound
msgid "Breaches found! Use Identity Monitor for details."
msgstr "Verstöße gefunden! Verwenden Sie Identity Monitor für Details."

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitortableusercaption
msgid "Monitored user (in Identity Monitor, you can set up more)"
msgstr "Überwachter Benutzer (in Identity Monitor können Sie weitere einrichten)"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelclean
msgid "Logon clean"
msgstr "Saubere Anmeldung"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfail
msgid "Failed"
msgstr "Gescheitert"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelfound
msgid "Breaches found!"
msgstr "Verstöße gefunden!"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname
msgctxt "spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheelproductname"
msgid "Identity Monitor"
msgstr "Identity Monitor"

#: spybot3.crossapplinks.identitymonitor.frame.rscrossapplinkidentitymonitorwheeltesting
msgid "Testing"
msgstr "Prüfung"

#: spybot3.settings.ui.rsdarkmodesettingalwaysoff
msgid "Always off"
msgstr "Immer aus"

#: spybot3.settings.ui.rsdarkmodesettingalwayson
msgid "Always on"
msgstr "Immer an"

#: spybot3.settings.ui.rsdarkmodesettingnighttime
msgid "On during night time"
msgstr "Nachts an"

#: spybot3.settings.ui.rsdarkmodesettingsystemsetting
msgid "Based on global system setting"
msgstr "Folge globaler Systemeinstellung"

#: spybot3.settings.ui.rsdarkmodesettingthreatlevel
msgid "Based on threat level"
msgstr "Basierend auf Bedrohungsstufe"

#: spybot3.settings.ui.rsproxymodecustomproxy
msgid "Use custom proxy"
msgstr "Benutzerdefinierten Proxy verwenden"

#: spybot3.settings.ui.rsproxymodenoproxy
msgid "Do not use a proxy"
msgstr "Keinen Proxy verwenden"

#: spybot3.settings.ui.rsproxymodesystemproxy
msgid "Use system proxy"
msgstr "System-Proxy verwenden"

#: spybot3.settings.ui.rssettingsalwaysshowconnections
msgctxt "spybot3.settings.ui.rssettingsalwaysshowconnections"
msgid "Always show connections"
msgstr "Verbindungen immer zeigen"

#: spybot3.settings.ui.rssettingsdarkmode
msgid "Dark Mode"
msgstr "Nachtmodus"

#: spybot3.settings.ui.rssettingsdebugloglog
msgid "Show debug information"
msgstr "Informationen zur Fehlersuche"

#: spybot3.settings.ui.rssettingslanguagesystemdefault
msgid "System default language"
msgstr "Standard-System-Sprache"

#: spybot3.settings.ui.rssettingsproxymode
msgid "Proxy"
msgstr "Proxy"

#: spybot3.settings.ui.rssettingsshowmainmenu
msgid "Show assistive menu on Ctrl+M"
msgstr "Hilfsmenü mit Strg+M anzeigen"

#: spybot3.settings.ui.rssettingsshownotifications
msgid "Show notifications"
msgstr "Benachrichtigungen zeigen"

#: spybot3.settings.ui.rssettingsusedyslexicfont
msgid "Improve text for dyslexic users"
msgstr "Text für Legastheniker optimieren"

#: spybot3.settings.ui.rssettingsuserinterfacelanguage
msgid "User interface language"
msgstr "Sprache für Benutzeroberfläche"

#: spybot3.settings.ui.rssettingsusespeechapi
msgid "Speak user interface elements"
msgstr "Benutzerlemente aussprechen"

#: spybot3searchengines.database.rssearchengineenergystandard
msgid "bod standard"
msgstr "Leibnorm"

#: spybot3searchengines.database.rssearchengineenergysustainable
msgid "sustainable"
msgstr "nachhaltig"

#: spybot3searchengines.database.rssearchengineprivacyfocus
msgid "privacy aware"
msgstr "Bewusstsein für die Privatsphäre"

#: spybot3searchengines.database.rssearchengineprivacystandard
msgid "standard"
msgstr "Standard"

#: spybot3searchengines.database.rssearchenginetypeinstalled
msgid "installed"
msgstr "installiert"

#: spybot3searchengines.database.rssearchenginetyperecommended
msgid "recommended"
msgstr "Empfohlen"

#: tformspybot3antibeacon.caption
msgid "Spybot AntiBeacon"
msgstr "Spybot AntiBeacon"
